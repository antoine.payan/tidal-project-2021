package Materiel_Usine
  connector Water_port
    Modelica.SIunits.Position h "Hauteur d'eau du système";
    flow Modelica.SIunits.VolumeFlowRate Q "Débit d'eau volumique traversant le système";
    annotation(
      Diagram(graphics = {Rectangle(origin = {-8, -2}, fillPattern = FillPattern.Solid, extent = {{-68, 60}, {68, -60}}), Text(origin = {-8, 0}, lineColor = {255, 255, 255}, fillColor = {255, 255, 255}, extent = {{-16, 10}, {16, -10}}, textString = "Port")}),
      Icon(graphics = {Rectangle(origin = {-8, -2}, fillPattern = FillPattern.Solid, extent = {{-68, 60}, {68, -60}}), Text(origin = {-8, 0}, lineColor = {255, 255, 255}, fillColor = {255, 255, 255}, extent = {{-16, 10}, {16, -10}}, textString = "Port")}));
  end Water_port;

  model Bassin_Lineaire "Bassin de stockage de l'eau de l'usine avec aire linéaire"
    Materiel_Usine.Water_port p annotation(
      Placement(visible = true, transformation(origin = {-126, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-128, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    // Propriétés du Bassin
    Modelica.SIunits.Height h "Niveau d'eau dans le bassin";
    Modelica.SIunits.Volume V "Volume d'eau dans le bassin";
    Modelica.SIunits.VolumeFlowRate Q "Débit d'eau entrant dans le bassin";
    // Paramètres géométriques
    //En V1 on considère un bassin rectangulaire et non de largeur affine (enlève non linéarités)
    parameter Modelica.SIunits.Area Aire(displayUnit = "km2") = 12e6 "Aire du bassin chosie aléatoirement";
    //Initialisation
    //On peut en faire une si on veut, valeur à choisir
    parameter Modelica.SIunits.Height h_init(min = 0) = 12 "Valeur initale de la hauteur d'eau" annotation(
      Dialog(tab = "Initialization"));
  initial equation
    h = h_init;
  equation
    h = p.h;
    Q = p.Q;
    V = Aire * h "Volume d'eau dans le bassin";
    Q = der(V);
    annotation(
      Diagram(graphics = {Rectangle(origin = {0, -29}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-60, 51}, {60, -51}}), Line(origin = {-60, 48}, points = {{0, 30}, {0, -30}, {0, -30}}, color = {0, 0, 255}), Line(origin = {-60, 48}, points = {{0, 30}, {0, -30}, {0, -30}}, color = {0, 0, 255}), Line(origin = {60, 50.5}, points = {{0, 29.5}, {0, -30.5}, {0, -22.5}}, color = {0, 0, 255})}),
      Icon(graphics = {Line(origin = {-61.8947, 48}, points = {{0, 30}, {0, -30}, {0, -30}}, color = {0, 0, 255}), Line(origin = {58.1053, 50.5}, points = {{0, 29.5}, {0, -30.5}, {0, -22.5}}, color = {0, 0, 255}), Line(origin = {-61.8947, 48}, points = {{0, 30}, {0, -30}, {0, -30}}, color = {0, 0, 255}), Rectangle(origin = {-2, -29}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-60, 51}, {60, -51}})}));
  end Bassin_Lineaire;

  model Bassin_Reel "On modélise le bassin avec un aire variant en fonction de la hauteur"
    // Propriétés du Bassin
    Modelica.SIunits.Height h "Niveau d'eau dans le bassin";
    Modelica.SIunits.Volume V "Volume d'eau dans le bassin";
    Modelica.SIunits.VolumeFlowRate Q(displayUnit = "hm3") "Débit entrant dans le bassin";
    //Initialisation
    //On peut en faire une si on veut, valeur à choisir
    parameter Modelica.SIunits.Height h_init(min = 0) = 12 "Valeur initiale de la hauteur" annotation(
      Dialog(tab = "Initialization"));
    Materiel_Usine.Water_port p annotation(
      Placement(visible = true, transformation(origin = {-56, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-128, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  initial equation
    h = h_init;
  equation
    h = p.h;
    Q = p.Q;
    V = 1.33e6 / 2 * h * h + 4.11e6 * h;
    Q = der(V);
    annotation(
      Icon(graphics = {Line(origin = {-69.7894, 48}, points = {{0, 30}, {0, -30}, {0, -30}}, color = {0, 0, 255}), Polygon(origin = {4, -30}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, points = {{-74, 50}, {74, 50}, {16, -50}, {-74, -50}, {-74, -48}, {-74, 50}}), Line(origin = {87.78, 39.55}, points = {{-9.77639, -19.5528}, {10.2236, 20.4472}, {2.22361, 4.44721}}, color = {0, 0, 255})}),
      experiment(StartTime = 0, StopTime = 100000, Tolerance = 1e-6, Interval = 200));
  end Bassin_Reel;

  model Modele_marees
    Materiel_Usine.Water_port p annotation(
      Placement(visible = true, transformation(origin = {-54, 78}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {130, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    import Modelica.Constants.pi;
    // Paramètres du modèle de marrée
    parameter Modelica.SIunits.Time Ta = 12.5 * 60 "Période de marée de 12 heures 30";
    parameter Modelica.SIunits.Height a = 7.5 "Amplitude des marées";
    parameter Modelica.SIunits.Height b = 7.2 "Valeur moyenne des marées";
    // Variable de hauteur
    Modelica.SIunits.Position h "Hauteur de la mer";
  equation
    h = a * cos(2 * time * pi / Ta) + b;
    h = p.h annotation(
      Icon(graphics = {Text(origin = {-10, 29}, extent = {{-50, 23}, {50, -23}}, textString = "Marées")}));
    annotation(
      Diagram(graphics = {Rectangle(origin = {0, -7}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-98, 21}, {98, -21}}), Polygon(origin = {-72, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {22, -11}, {22, -15}, {-26, -11}}), Polygon(origin = {-22, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {24, -11}, {26, -15}, {-26, -11}}), Polygon(origin = {26, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-24, -11}, {14, 11}, {24, -11}, {12, -27}, {-24, -11}}), Polygon(origin = {76, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {22, -11}, {20, -17}, {-26, -11}})}),
      Icon(graphics = {Polygon(origin = {-72, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {22, -11}, {22, -15}, {-26, -11}}), Rectangle(origin = {0, -7}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-98, 21}, {98, -21}}), Polygon(origin = {26, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-24, -11}, {14, 11}, {24, -11}, {12, -27}, {-24, -11}}), Polygon(origin = {76, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {22, -11}, {20, -17}, {-26, -11}}), Polygon(origin = {-22, 25}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Sphere, points = {{-26, -11}, {14, 11}, {24, -11}, {26, -15}, {-26, -11}})}),
      experiment(StartTime = 0, StopTime = 1000, Tolerance = 1e-6, Interval = 1.66667));
  end Modele_marees;

  model Assemblage
    Materiel_Usine.Bassin_Reel bassin_Reel annotation(
      Placement(visible = true, transformation(origin = {74, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Materiel_Usine.Usine usine annotation(
      Placement(visible = true, transformation(origin = {0, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Materiel_Usine.Modele_marees modele_marees annotation(
      Placement(visible = true, transformation(origin = {-74, -10}, extent = {{-12, -12}, {12, 12}}, rotation = 0)));
  equation
    connect(modele_marees.p, usine.water_port) annotation(
      Line(points = {{-58, -10}, {-14, -10}, {-14, -8}}));
    connect(bassin_Reel.p, usine.water_port1) annotation(
      Line(points = {{61, -10}, {12, -10}}));
    annotation(
      experiment(StartTime = 0, StopTime = 2000, Tolerance = 1e-06, Interval = 4));
  end Assemblage;

  model Vanne "Modèle des 6 vannes de l'usine, permettant de remplir rapidement le bassin"
    parameter Real n = 6 "Nombre de vanne dans l'usine";
    parameter Real Beta = 1.51e-2 "Paramètre linéaire de la relation Hauteur d'eau et Débit";
    Materiel_Usine.Water_port water_port_mer annotation(
      Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-126, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Materiel_Usine.Water_port water_port_bassin annotation(
      Placement(visible = true, transformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {124, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.SIunits.Height Hb;
    Modelica.SIunits.Height Hm;
    Modelica.SIunits.Height H;
    Modelica.SIunits.VolumeFlowRate Q_vanne "débit passant à travers les vannes";
    Integer commande "commande d'état reçue du contrôleur";
    Modelica.Blocks.Interfaces.IntegerInput info_port annotation(
      Placement(visible = true, transformation(origin = {-64, 118}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-40, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  equation
    water_port_bassin.h = Hb;
    water_port_mer.h = Hm;
    H = Hb - Hm;
    commande = info_port;
    if commande == 1 then
      Q_vanne = H / Beta * 4;
    else
      Q_vanne = 0;
    end if;
    water_port_bassin.Q = n * Q_vanne;
    water_port_mer.Q = -n * Q_vanne;
    annotation(
      Diagram(graphics = {Rectangle(fillColor = {97, 97, 97}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-98, 28}, {98, -28}}), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {-1.33544, 53.3354}, points = {{-26, -4}, {26, 4}}, color = {238, 0, 0}, thickness = 1.5), Line(origin = {0, 40}, points = {{0, 12}, {0, -12}}, thickness = 2.25), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Ellipse(origin = {0, 54}, lineColor = {245, 0, 0}, lineThickness = 3, extent = {{-30, 10}, {30, -10}}, endAngle = 360), Text(origin = {-1, -40}, extent = {{-37, 10}, {37, -10}}, textString = "Vannes")}),
      Icon(graphics = {Line(origin = {-1.33544, 53.3354}, points = {{-26, -4}, {26, 4}}, color = {238, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Rectangle(fillColor = {97, 97, 97}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-98, 28}, {98, -28}}), Ellipse(origin = {0, 54}, lineColor = {245, 0, 0}, lineThickness = 3, extent = {{-30, 10}, {30, -10}}, endAngle = 360), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 40}, points = {{0, 12}, {0, -12}}, thickness = 2.25), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Line(origin = {0, 53}, points = {{-10, 9}, {10, -9}}, color = {239, 0, 0}, thickness = 1.5), Text(origin = {-1, -40}, extent = {{-37, 10}, {37, -10}}, textString = "Vannes")}));
  end Vanne;

  connector Info_port
    Integer Commande "Commande de fonctionnement du système";
    annotation(
      Icon(graphics = {Rectangle(origin = {-1, 3}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-49, 35}, {49, -35}})}));
  end Info_port;

  model controleur
    Materiel_Usine.Water_port water_port_mer annotation(
      Placement(visible = true, transformation(origin = {-112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Water_port water_port_bassin annotation(
      Placement(visible = true, transformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.SIunits.Height H;
    Modelica.SIunits.Height Hb;
    Modelica.SIunits.Height Hm;
    parameter Modelica.SIunits.VolumeFlowRate Q_controleur = 0 "Débit passant à travers le contrôleur, soit 0";
    Integer commande "commande d'état donnée par le controleur";
    Modelica.Blocks.Interfaces.IntegerOutput info_port annotation(
      Placement(visible = true, transformation(origin = {118, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  initial equation
    commande = 1;
  equation
    water_port_bassin.h = Hb;
    water_port_mer.h = Hm;
    H = Hb - Hm;
    water_port_bassin.Q = Q_controleur;
    water_port_mer.Q = -Q_controleur;
    info_port = commande;
  algorithm
    when pre(commande) == 1 and der(Hm) <= 0 then
      commande := 2 "On passe dans l'état 2 = fermeture des vannes et début du pompage";
    end when;
    when pre(commande) == 2 and H > 6 then
      commande := 3 "On passe dans l'état 3 = arrêt des pompes";
    end when;
    when pre(commande) == 3 and H > 11 then
      commande := 4 "On passe dans l'état 4 = début du turbinage";
    end when;
    when pre(commande) == 4 and H < 1 then
      commande := 5 "on passe dans l'état 5 = fin du turbinage";
    end when;
    when pre(commande) == 5 and H <= 0 then
      commande := 1 "On revient dans l'état 1 = ouverture des vannes";
    end when;
    annotation(
      Icon(graphics = {Rectangle(origin = {-0.18, -0.18}, fillColor = {181, 181, 181}, fillPattern = FillPattern.Solid, extent = {{-79.82, 60.18}, {79.82, -60.18}}), Rectangle(origin = {-0.02, -1}, fillColor = {170, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-59.98, 39}, {59.98, -39}}), Text(origin = {0, -76}, extent = {{-52, 12}, {52, -12}}, textString = "Contrôleur")}));
  end controleur;

  model Turbine
    Water_port water_port_mer annotation(
      Placement(visible = true, transformation(origin = {-112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Water_port water_port_bassin annotation(
      Placement(visible = true, transformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    parameter Real n = 24 "Nombre de turbines dans l'usine";
    parameter Real Rendement = 0.95 "Rendement d'une turbine de l'usine";
    parameter Real coeff_turbinage = 25 "Modélisation linéraire du débit Débit = coeff* H";
    parameter Modelica.SIunits.VolumeFlowRate Q_pompage = n * 200 "Débit de pompage constant égal à 2/3 du débit maximum (arbitraire)";
    parameter Modelica.SIunits.Density rho = 1000;
    parameter Modelica.SIunits.Acceleration g = 9.81;
    Modelica.SIunits.Height Hb;
    Modelica.SIunits.Height Hm;
    Modelica.SIunits.Height H;
    Modelica.SIunits.VolumeFlowRate Q_turbine "Débit traversant les turbines";
    Integer commande "Commande d'état reçue depuis le contrôleur";
    Modelica.SIunits.Power P(displayUnit = "MW") "Puissance électrique produite par l'usine";
    Modelica.Blocks.Interfaces.IntegerInput info_port annotation(
      Placement(visible = true, transformation(origin = {-64, 118}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-40, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  equation
    water_port_bassin.h = Hb;
    water_port_mer.h = Hm;
    H = Hb - Hm;
    commande = info_port;
    if commande == 2 then
      Q_turbine = -Q_pompage;
    elseif commande == 4 then
      Q_turbine = coeff_turbinage * H;
    else
      Q_turbine = 0;
    end if;
    P = -Rendement * rho * g * H * Q_turbine;
    water_port_mer.Q = -n * Q_turbine;
    water_port_bassin.Q = n * Q_turbine;
    annotation(
      Diagram(graphics = {Polygon(origin = {0, 50}, fillColor = {221, 221, 221}, fillPattern = FillPattern.Solid, points = {{-34, 30}, {-12, -28}, {12, -28}, {34, 30}, {-20, 30}, {-34, 30}}), Polygon(origin = {42, -41}, fillColor = {227, 227, 227}, fillPattern = FillPattern.Solid, points = {{-28, 21}, {-18, 43}, {44, 29}, {4, -29}, {-28, 21}}), Polygon(origin = {-39, -36}, fillColor = {231, 231, 231}, fillPattern = FillPattern.Solid, points = {{-29, -2}, {-7, -32}, {29, 20}, {17, 38}, {-47, 24}, {-29, -2}}), Ellipse(origin = {0, 1}, fillColor = {156, 156, 156}, fillPattern = FillPattern.Sphere, extent = {{-26, 25}, {26, -25}}, endAngle = 360), Text(origin = {1, -48}, extent = {{-23, 8}, {23, -8}}, textString = "Turbines")}),
      Icon(graphics = {Polygon(origin = {0, 50}, fillColor = {221, 221, 221}, fillPattern = FillPattern.Solid, points = {{-34, 30}, {-12, -28}, {12, -28}, {34, 30}, {-20, 30}, {-34, 30}}), Polygon(origin = {42, -41}, fillColor = {227, 227, 227}, fillPattern = FillPattern.Solid, points = {{-28, 21}, {-18, 43}, {44, 29}, {4, -29}, {-28, 21}}), Polygon(origin = {-39, -36}, fillColor = {231, 231, 231}, fillPattern = FillPattern.Solid, points = {{-29, -2}, {-7, -32}, {29, 20}, {17, 38}, {-47, 24}, {-29, -2}}), Text(origin = {1, -48}, extent = {{-23, 8}, {23, -8}}, textString = "Turbines"), Ellipse(origin = {0, 1}, fillColor = {156, 156, 156}, fillPattern = FillPattern.Sphere, extent = {{-26, 25}, {26, -25}}, endAngle = 360)}));
  end Turbine;

  model Usine
    Materiel_Usine.Water_port water_port annotation(
      Placement(visible = true, transformation(origin = {-90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-82, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Materiel_Usine.Water_port water_port1 annotation(
      Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {90, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Materiel_Usine.Turbine turbine annotation(
      Placement(visible = true, transformation(origin = {3.55271e-15, -60}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
    Materiel_Usine.Vanne vanne annotation(
      Placement(visible = true, transformation(origin = {21, -1}, extent = {{-17, -17}, {17, 17}}, rotation = 0)));
    Materiel_Usine.controleur controleur annotation(
      Placement(visible = true, transformation(origin = {-8, 68}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  equation
    connect(water_port, turbine.water_port_mer) annotation(
      Line(points = {{-90, 0}, {-40, 0}, {-40, -60}, {-20, -60}}));
    connect(water_port1, turbine.water_port_bassin) annotation(
      Line(points = {{90, 0}, {60, 0}, {60, -60}, {20, -60}}));
    connect(water_port1, vanne.water_port_bassin) annotation(
      Line(points = {{90, 0}, {42, 0}}));
    connect(water_port, vanne.water_port_mer) annotation(
      Line(points = {{-90, 0}, {0, 0}}));
    connect(water_port, controleur.water_port_mer) annotation(
      Line(points = {{-90, 0}, {-40, 0}, {-40, 68}, {-29, 68}}));
    connect(water_port1, controleur.water_port_bassin) annotation(
      Line(points = {{90, 0}, {60, 0}, {60, 68}, {14, 68}}));
    connect(controleur.info_port, vanne.info_port) annotation(
      Line(points = {{-8, 46}, {14, 46}, {14, 20}}, color = {255, 127, 0}));
    connect(controleur.info_port, turbine.info_port) annotation(
      Line(points = {{-8, 46}, {-8, -38}}, color = {255, 127, 0}));
    annotation(
      experiment(StartTime = 0, StopTime = 1000, Tolerance = 1e-6, Interval = 2),
      Icon(graphics = {Rectangle(origin = {-0.22, -25.45}, fillPattern = FillPattern.Solid, extent = {{-59.78, 35.45}, {59.78, -35.45}}), Rectangle(origin = {-35, -24}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-9, 10}, {9, -10}}), Rectangle(origin = {31, -24}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-9, 10}, {9, -10}}), Rectangle(origin = {-1, -24}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-9, 10}, {9, -10}}), Polygon(origin = {-40, 17}, fillPattern = FillPattern.Solid, points = {{-20, -7}, {20, 11}, {20, -11}, {-20, -7}}), Polygon(origin = {-40, 17}, fillPattern = FillPattern.Solid, points = {{-20, -7}, {20, 11}, {20, -11}, {-20, -7}}), Polygon(origin = {0, 17}, fillPattern = FillPattern.Solid, points = {{-20, -7}, {20, 11}, {20, -11}, {-20, -7}}), Polygon(origin = {40, 17}, fillPattern = FillPattern.Solid, points = {{-20, -7}, {20, 11}, {20, -11}, {-20, -7}}), Rectangle(origin = {-45, 38}, fillPattern = FillPattern.VerticalCylinder, extent = {{-7, 32}, {7, -32}}), Text(origin = {1, -51}, lineColor = {255, 88, 5}, extent = {{-23, 7}, {23, -7}}, textString = "EDF")}));
  end Usine;

  package Tests
    model Source_de_debit
      parameter Modelica.SIunits.VolumeFlowRate Q = 1000 "Débit d'eau entrant dans le bassin";
      Materiel_Usine.Water_port p annotation(
        Placement(visible = true, transformation(origin = {46, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {122, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      -Q = p.Q;
      annotation(
        Icon(graphics = {Ellipse(origin = {3, -1}, fillColor = {0, 0, 127}, fillPattern = FillPattern.Sphere, extent = {{-41, 41}, {41, -41}}, endAngle = 360)}));
    end Source_de_debit;

    model Testeur_Bassin_lineaire
      Materiel_Usine.Bassin_Lineaire bassin_Lineaire annotation(
        Placement(visible = true, transformation(origin = {38, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Source_de_debit source_de_debit annotation(
        Placement(visible = true, transformation(origin = {-58, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(source_de_debit.p, bassin_Lineaire.p) annotation(
        Line(points = {{-46, 6}, {25, 6}}));
      annotation(
        experiment(StartTime = 0, StopTime = 100000, Tolerance = 1e-6, Interval = 200));
    end Testeur_Bassin_lineaire;

    model Testeur_Bassin_Reel
      Source_de_debit source_de_debit annotation(
        Placement(visible = true, transformation(origin = {-66, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Materiel_Usine.Bassin_Reel bassin_Reel annotation(
        Placement(visible = true, transformation(origin = {40, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(source_de_debit.p, bassin_Reel.p) annotation(
        Line(points = {{-54, 10}, {28, 10}, {28, 11}}));
    end Testeur_Bassin_Reel;

    model Hauteur_eau_constante
      parameter Modelica.SIunits.Height H = 45 "Hauteur d'eau fixée";
      Materiel_Usine.Water_port p annotation(
        Placement(visible = true, transformation(origin = {-12, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {116, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      p.h = H;
      annotation(
        Icon(graphics = {Line(origin = {5.85355, -15}, points = {{-69.8536, 63}, {-5.85355, 63}, {-5.85355, -63}, {70.1464, -63}, {68.1464, -61}})}));
    end Hauteur_eau_constante;

    package Test_vannes
      model Test_vannes
        Commande_test_vannes commande_test_vannes;
        Bassin_Lineaire bassin_test_vannes;
        Mer_test_vannes mer_test_vannes;
        Vanne vannes;
      equation
        connect(commande_test_vannes.info_port, vannes.info_port);
        connect(vannes.water_port_mer, mer_test_vannes.p);
        connect(vannes.water_port_bassin, bassin_test_vannes.p);
        annotation(
          experiment(StartTime = 0, StopTime = 100000, Tolerance = 1e-6, Interval = 200));
      end Test_vannes;

      model Commande_test_vannes
        Integer commande "commande de sortie du contrôleur";
        Modelica.Blocks.Interfaces.IntegerOutput info_port annotation(
          Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {30, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      equation
        commande = if time < 500 then 1 else 0;
        info_port = commande;
        annotation(
          Diagram);
      end Commande_test_vannes;

      model Bassin_test_vannes
        Materiel_Usine.Water_port p;
        // Propriétés du Bassin
        Modelica.SIunits.Height h "Niveau d'eau dans le bassin";
        Modelica.SIunits.Volume V "Volume d'eau dans le bassin";
        Modelica.SIunits.VolumeFlowRate Q "Débit d'eau entrant dans le bassin";
        parameter Modelica.SIunits.Area Aire(displayUnit = "km2") = 12e6 "Aire du bassin chosie aléatoirement";
        //On peut en faire une si on veut, valeur à choisir
      initial equation
        h = 1;
      equation
        h = p.h;
        V = Aire * h "Volume d'eau dans le bassin";
        Q = der(V);
        Q = p.Q;
        annotation(
          experiment(StartTime = 0, StopTime = 1000000, Tolerance = 1e-6, Interval = 2000));
      end Bassin_test_vannes;

      model Mer_test_vannes
        Water_port p;
        Real hm = 12 "hauteur d'eau de la mer";
      equation
        p.h = hm;
      end Mer_test_vannes;

      model Test_vannes_Bassin_Reel
        Materiel_Usine.Vanne Vanne annotation(
          Placement(visible = true, transformation(origin = {-10, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Materiel_Usine.Bassin_Reel bassin_Reel annotation(
          Placement(visible = true, transformation(origin = {60, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Materiel_Usine.Tests.Hauteur_eau_constante hauteur_eau_constante annotation(
          Placement(visible = true, transformation(origin = {-66, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Materiel_Usine.Tests.Test_vannes.Commande_test_vannes commande_test_vannes annotation(
          Placement(visible = true, transformation(origin = {-16, 48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      equation
        connect(Vanne.water_port_bassin, bassin_Reel.p) annotation(
          Line(points = {{2, -13}, {48, -13}}));
        connect(Vanne.water_port_mer, hauteur_eau_constante.p) annotation(
          Line(points = {{-22, -14}, {-54, -14}}));
        connect(commande_test_vannes.info_port, Vanne.info_port) annotation(
          Line(points = {{-13, 37}, {-14, 37}, {-14, -2}}, color = {255, 127, 0}));
        annotation(
          Diagram,
          experiment(StartTime = 0, StopTime = 1000, Tolerance = 1e-06, Interval = 2));
      end Test_vannes_Bassin_Reel;

      model Commande_test_turbine
        Integer commande "commande de sortie du contrôleur";
        Modelica.Blocks.Interfaces.IntegerOutput info_port annotation(
          Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {30, -110}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      equation
        commande = if time < 500 then 4 else 0;
        info_port = commande;
        annotation(
          Diagram);
      end Commande_test_turbine;
    end Test_vannes;

    model Test_Turbine
      Materiel_Usine.Tests.Hauteur_eau_constante hauteur_eau_constante annotation(
        Placement(visible = true, transformation(origin = {-66, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Materiel_Usine.Bassin_Reel bassin_Reel annotation(
        Placement(visible = true, transformation(origin = {62, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Materiel_Usine.Tests.Test_vannes.Commande_test_turbine commande_test_turbine annotation(
        Placement(visible = true, transformation(origin = {4, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Turbine turbine annotation(
        Placement(visible = true, transformation(origin = {-6, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(hauteur_eau_constante.p, turbine.water_port_mer) annotation(
        Line(points = {{-54, -10}, {-18, -10}, {-18, -12}}));
      connect(bassin_Reel.p, turbine.water_port_bassin) annotation(
        Line(points = {{50, -14}, {6, -14}, {6, -12}}));
      connect(turbine.info_port, commande_test_turbine.info_port) annotation(
        Line(points = {{-10, 0}, {7, 0}, {7, 41}}, color = {255, 127, 0}));
      annotation(
        experiment(StartTime = 0, StopTime = 1000, Tolerance = 1e-06, Interval = 2));
    end Test_Turbine;

    package test_controleur
      model Tets_controleur
        Materiel_Usine.controleur controleur annotation(
          Placement(visible = true, transformation(origin = {-1, -1}, extent = {{-21, -21}, {21, 21}}, rotation = 0)));
        Materiel_Usine.Tests.test_controleur.mer_test_controleur mer_test_controleur annotation(
          Placement(visible = true, transformation(origin = {-77, -1}, extent = {{-17, -17}, {17, 17}}, rotation = 0)));
        Materiel_Usine.Tests.test_controleur.bassin_test_controleur bassin_test_controleur annotation(
          Placement(visible = true, transformation(origin = {76, 0}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
        Vanne vanne annotation(
          Placement(visible = true, transformation(origin = {-4, -58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      equation
        connect(mer_test_controleur.water_port, controleur.water_port_mer) annotation(
          Line(points = {{-58, 0}, {-24, 0}}));
        connect(controleur.water_port_bassin, bassin_test_controleur.water_port) annotation(
          Line(points = {{22, 0}, {55, 0}}));
        connect(mer_test_controleur.water_port, vanne.water_port_mer) annotation(
          Line(points = {{-58, 0}, {-16, 0}, {-16, -58}}));
        connect(controleur.water_port_bassin, vanne.water_port_bassin) annotation(
          Line(points = {{22, 0}, {8, 0}, {8, -58}}));
        connect(controleur.info_port, vanne.info_port) annotation(
          Line(points = {{0, -24}, {-8, -24}, {-8, -46}}, color = {255, 127, 0}));
        annotation(
          experiment(StartTime = 0, StopTime = 1000, Tolerance = 1e-6, Interval = 2));
      end Tets_controleur;

      model mer_test_controleur
        Water_port water_port annotation(
          Placement(visible = true, transformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {112, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Modelica.SIunits.Height h "hauteur d'eau de la mer";
        Modelica.SIunits.VolumeFlowRate Q "débit passsant par le controleur";
      equation
        h = 2;
        water_port.h = h;
        water_port.Q = Q;
      end mer_test_controleur;

      model bassin_test_controleur
        Materiel_Usine.Water_port water_port annotation(
          Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-114, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Modelica.SIunits.Height h "hauteur d'eau du bassin";
        Modelica.SIunits.VolumeFlowRate Q "débit passsant par le correcteur";
      equation
        if time > 100 then
          h = 2;
        else
          h = 4;
        end if;
        water_port.h = h;
        water_port.Q = Q;
      equation

      end bassin_test_controleur;

      model composant_commande_test_controleur
        Materiel_Usine.Info_port info_port annotation(
          Placement(visible = true, transformation(origin = {0, 112}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 104}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
        Integer commande;
      equation
        info_port.Commande = commande;
      end composant_commande_test_controleur;
    end test_controleur;
  end Tests;
  annotation(
    uses(Modelica(version = "3.2.3")));
end Materiel_Usine;
