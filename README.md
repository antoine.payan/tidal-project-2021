# Usine marémotrice de la Rance

Le projet consiste à modéliser l'usine marémotrice située sur l'estuaire de la Rance. 

Auteurs : Chana Boulanger, Paul-Hubert Cartier & Antoine Payan

### Le projet
L'usine marémotrice motrice de la Rance, en Ille-et Vilaine est une des plus grandes usines marémotrices au monde, avec une capacité installée de 240 MW. Pour modéliser son fonctionnement, il a fallu modéliser, en plus de l'usine,  l'action des marées et le bassin que constitue l'estuaire de la Rance.


### Composition du code
Ce code est organisé au sein d'un package 'Materiel_Usine', qui contient :
* le modèle de simulation 'Assemblage'
Les composants principaux de ce modèle Assemblage sont :
* le modèle du bassin 'Bassin_Reel'
* le modèle des marées 'Modele_marees'
* le modèle de l'usine 'Usine'
Les autres modèles présents dans le package sont les différents composants de ces modèles principaux :
* Les vannes 'Vanne', qui sert à modéliser les 6 vannes de l'usine.
* Le contrôleur 'controleur', qui permet de modéliser le fonctionnement en simple effet de l'usine
* La turbine 'Turbine', qui sert à modéliser les 24 turbines de l'usine, en fonctionnement turbines ou pompes.
* Le port 'Water_port" pour les mouvements d'eau au sein de l'usine (Flux=h (débit volumique), Potentiel=h (hauteur d'eau)).
* Le port Info_port pour envoyer les commandes du contrôleur aux vannes et aux turbines.
* un package Tests, contenant l'ensemble des tests et des composants nécessaires à ces tests

### Utilisation

Vous pouvez faire simuler différent modèles.
En premier lieu de modèle général qui symbolise l'usine au complet.
En second lieu différents tests de composants:
 * Le test des Vannes avec le fichier Test_Vannes_Bassin_Réel qui montre les vannes soumises a une mer constante et a un commande variable
 * Le test des turbines dans le fichier Test_Turbines qui montre les turbines soumises a une mer constante et une variation de commande